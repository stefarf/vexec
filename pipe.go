package vexec

import (
	"fmt"
	"io"
	"os/exec"
	"strings"

	"gitlab.com/stefarf/iferr"
)

type Pipe struct {
	ex       *Exec
	cmds     []*exec.Cmd
	printCmd bool
}

func (ex *Exec) StartPipe() *Pipe {
	return &Pipe{ex: ex}
}

func (p *Pipe) SetPrintCmd(print bool) *Pipe {
	p.printCmd = print
	return p
}

func (p *Pipe) RunCmdArgs(cmd string, args ...string) *Pipe {
	pipe := ""
	if len(p.cmds) != 0 {
		pipe = "| "
	}
	longCmd := strings.Join(append([]string{cmd}, args...), " ")
	if p.printCmd {
		fmt.Printf("\n%s $ %s%s\n", p.ex.dir, pipe, longCmd)
	}

	c := exec.Command(cmd, args...)
	c.Dir = p.ex.dir
	c.Env = p.ex.env
	c.Stdout = p.ex.stdout
	c.Stderr = p.ex.stderr
	p.cmds = append(p.cmds, c)
	return p
}

func (p *Pipe) Run(longCmd string) *Pipe {
	s := strings.Split(longCmd, " ")
	return p.RunCmdArgs(s[0], s[1:]...)
}

func (p *Pipe) EndPipe() *Exec {
	if p.ex.Error != nil {
		return p.ex
	}
	p.ex.Error = runPipes(p.cmds, p.ex.stdout, p.ex.stderr)
	return p.ex
}

func runPipes(cmds []*exec.Cmd, stdout, stderr io.Writer) error {
	if len(cmds) <= 1 {
		panic("must run at least 2 commands")
	}

	rList := make([]*io.PipeReader, len(cmds))
	wList := make([]*io.PipeWriter, len(cmds))

	i, j := 0, 1
	for j < len(cmds) {
		c1, c2 := cmds[i], cmds[j]

		r2, w1 := io.Pipe()
		c1.Stdout = w1
		c2.Stdin = r2

		rList[j] = r2
		wList[i] = w1

		i++
		j++
	}

	cmds[len(cmds)-1].Stdout = stdout
	cmds[len(cmds)-1].Stderr = stderr

	for _, cmd := range cmds {
		err := cmd.Start()
		if err != nil {
			return err
		}
	}

	for i := 0; i < len(cmds); i++ {
		iferr.Print(cmds[i].Wait())
		if rList[i] != nil {
			iferr.Print(rList[i].Close())
		}
		if wList[i] != nil {
			iferr.Print(wList[i].Close())
		}
	}

	return nil
}
