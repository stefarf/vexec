package vexec

import (
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/stefarf/vexec/writerparser/writeronlywp"
)

type Exec struct {
	env      []string
	stdin    io.Reader
	stdout   WriterParser
	stderr   WriterParser
	dir      string
	printCmd bool

	Error error
}

type WriterParser interface {
	io.Writer
	Parse() string
}

func New() *Exec {
	return &Exec{
		env:    os.Environ(),
		stdout: writeronlywp.New(os.Stdout),
		stderr: writeronlywp.New(os.Stderr),
	}
}

func (ex *Exec) IferrFatal() *Exec {
	if ex.Error != nil {
		log.Fatal(ex.Error)
	}
	return ex
}

func (ex *Exec) IferrPrint() *Exec {
	if ex.Error != nil {
		log.Println(ex.Error)
	}
	return ex
}

func (ex *Exec) IferrPrintStderr() *Exec {
	if ex.Error != nil {
		_, _ = ex.stderr.Write([]byte(fmt.Sprint(ex.Error)))
	}
	return ex
}

func (ex *Exec) ClearError() *Exec {
	ex.Error = nil
	return ex
}

func (ex *Exec) SetEnv(key, value string) *Exec {
	ex.env = append(ex.env, fmt.Sprintf("%s=%s", key, value))
	return ex
}

func (ex *Exec) ChDir(dir string) *Exec {
	ex.dir = dir
	return ex
}

func (ex *Exec) RunCmdArgs(cmd string, args ...string) *Exec {
	if ex.Error != nil {
		return ex
	}

	longCmd := strings.Join(append([]string{cmd}, args...), " ")
	if ex.printCmd {
		fmt.Printf("\n%s $ %s\n", ex.dir, longCmd)
	}

	c := exec.Command(cmd, args...)
	c.Dir = ex.dir
	c.Env = ex.env
	c.Stdin = ex.stdin
	c.Stdout = ex.stdout
	c.Stderr = ex.stderr

	defer func() {
		ex.stdin = nil
	}()

	err := c.Run()
	if err != nil {
		ex.Error = err
		return ex
	}
	return ex
}

func (ex *Exec) Run(longCmd string) *Exec {
	if ex.Error != nil {
		return ex
	}
	s := strings.Split(longCmd, " ")
	return ex.RunCmdArgs(s[0], s[1:]...)
}

func (ex *Exec) SetPrintCmd(print bool) *Exec {
	ex.printCmd = print
	return ex
}

func (ex *Exec) SetStringAsStdin(s string) *Exec {
	ex.stdin = strings.NewReader(s)
	return ex
}

func (ex *Exec) SetStdout(wp WriterParser) *Exec {
	ex.stdout = wp
	return ex
}

func (ex *Exec) SetStderr(wp WriterParser) *Exec {
	ex.stderr = wp
	return ex
}

func (ex *Exec) ClearStdout() *Exec {
	ex.stdout = writeronlywp.New(os.Stdout)
	return ex
}

func (ex *Exec) ClearStderr() *Exec {
	ex.stderr = writeronlywp.New(os.Stderr)
	return ex
}

func (ex *Exec) ParseAndClearStdout() string {
	if ex.Error != nil {
		log.Println(ex.Error)
		return ""
	}
	defer func() {
		ex.stdout = writeronlywp.New(os.Stdout)
	}()
	return ex.stdout.Parse()
}

func (ex *Exec) ParseAndClearStderr() string {
	if ex.Error != nil {
		log.Println(ex.Error)
		return ""
	}
	defer func() {
		ex.stderr = writeronlywp.New(os.Stderr)
	}()
	return ex.stderr.Parse()
}

func (ex *Exec) ParseAndClearStdoutIfEmptyFatal() string {
	if ex.Error != nil {
		log.Println(ex.Error)
		return ""
	}
	rst := ex.ParseAndClearStdout()
	if rst == "" {
		log.Fatal("empty stdout parser output")
	}
	return rst
}

func (ex *Exec) ParseAndClearStderrIfEmptyFatal() string {
	if ex.Error != nil {
		log.Println(ex.Error)
		return ""
	}
	rst := ex.ParseAndClearStderr()
	if rst == "" {
		log.Fatal("empty stderr parser output")
	}
	return rst
}
