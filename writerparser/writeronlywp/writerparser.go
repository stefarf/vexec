package writeronlywp

import (
	"io"
)

type WriterParser struct{ wr io.Writer }

func New(wr io.Writer) *WriterParser                       { return &WriterParser{wr} }
func (wp *WriterParser) Write(p []byte) (n int, err error) { return wp.wr.Write(p) }
func (wp *WriterParser) Parse() string                     { return "" }
