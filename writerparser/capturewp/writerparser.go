package capturewp

import (
	"bytes"
	"strings"
)

type WriterParser struct{ buf bytes.Buffer }

func New() *WriterParser                                   { return &WriterParser{} }
func (wp *WriterParser) Write(p []byte) (n int, err error) { return wp.buf.Write(p) }
func (wp *WriterParser) Parse() string                     { return strings.TrimSpace(wp.buf.String()) }
